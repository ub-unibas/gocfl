package main

import (
	"go.ub.unibas.ch/gocfl/v2/ocflmain/cmd"
)

func main() {
	cmd.Execute()
}
